/*
 *Date: 2/12/14
 *Program an assignment to take 5 numbers and 
 *return their sum, average, min, max, median, mode,
 */
import java.util.Scanner;
import java.util.*;
public class Assignment2 {
	  public static void main(String[] agrs) {
		/*enter input*/
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the 5 random numbers, press ENTER after each input:");
		double num1 = keyboard.nextDouble();
		double num2 = keyboard.nextDouble();
		double num3 = keyboard.nextDouble();
		double num4 = keyboard.nextDouble();
		double num5 = keyboard.nextDouble();		
		
		/*find sum*/
		double sum = num1+num2+num3+num4+num5;
		System.out.println("Sum is: " + sum);
		
		/*find average*/
		double ave = sum/5;
		System.out.println("Mean is: " + ave);
		
		/*define & sort array*/
		double [] numbers = {num1, num2, num3, num4, num5};
		Arrays.sort(numbers);
		
		/*print max, min, med*/
		System.out.println("Minimum is " + numbers[0]);
		System.out.println("Maximum is " + numbers[4]);
		System.out.println("Median is " + numbers[2]);
		
		/*find mode by counting the number of appearance in the array*/
		int count = 0; 
		int mode = 1;
		double modeValue = 3.14159;
		for (int i = 0; i < 5; i++) {	
			for (int j = 0; j < 5; j++) { 
				if (numbers[i] == numbers[j]) count++;
			}
		/*record value if number[i] appears more frequently than the previous*/
		if (count > mode) {
			mode = count;
			modeValue = numbers[i];
		}
		/*special cases*/
		if (count == mode) {
			if (count !=1 && numbers[i] == modeValue); /*case: if a number appears more than twice, keep the same mode value*/
			else modeValue = 3.14159; /*case: if 2 numbers appear twice or no number appear more than once, no mode!*/
		}
		count = 0; /*restart counting*/
		}
		/*return mode value or none*/
		if (modeValue == 3.14159) System.out.println("No Mode");
		else System.out.println("Mode is " + modeValue);
	}
}
